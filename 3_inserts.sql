INSERT INTO person(personid, name, surname, birthday, telephone, email, address) VALUES
    (DEFAULT, 'Ramon', 'Ebneter', '1997-09-16', 0774220415, 'ramon.ebneter@bluewin.ch', 'Kaustrasse 46'),
    (DEFAULT, 'Roland', 'Bachmann', '1980-02-01', 0794532568, 'bachmann@outlook.com', 'Dorfstrasse 37'),
    (DEFAULT, 'Sabine', 'Ogler', '2000-04-14', 0774556049, 'oegler@hallo.com', 'Im Hause 7'),
    (DEFAULT, 'Katharina', 'Obleja', '1984-07-05', 0785644390, 'katharina1984@yahoo.com', 'Unteren Bachen 5'),
    (DEFAULT, 'Ralf', 'Stuebi', '1960-01-01', 0715608899, 'ralf_stuebi@bluewin.ch', 'Stadthaus 7'),
    (DEFAULT, 'Isabella', 'Suess', '1997-06-05', 0785617891, 'isabella_sueess@gmx.ch', 'Weidenstrasse 99'),
    (DEFAULT, 'Ruedi', 'Meister', '1965-11-01', 0412434569, 'ruedi.meister@hello.de', 'Im Haagen 591'),
    (DEFAULT, 'Roland', 'Brack', '1971-07-15', 0794220950, 'roland.brack@galaxus.ch', 'Blauringenstrasse 4'),
    (DEFAULT, 'Lara', 'Blakir', '1992-09-01', 0794324231, 'lara@blakir.ch', 'Ilgenstrasse 1'),
    (DEFAULT, 'Laura', 'Lavanga', '1999-05-01', 0784220000, 'laura.lavanga@sgkb.ch', 'Dorfstrasse 59'),
    (DEFAULT, 'Hans', 'Heiri', '1920-01-01', 0798500909, 'hansheiri@bluewin.ch', 'Dorfstrasse 99'),
    (DEFAULT, 'Tabea', 'Jung', '1976-06-18', 0764507711, 'tabea@gmail.com', 'Haerterei'),
    (DEFAULT, 'Salkararaan', 'Lijepataran', '1955-12-30', 0793462321, 'salin@kalin.ch', 'Rudolneuerstrasse 29');

INSERT INTO customer(personid, newsletter) VALUES
    (7, 'TRUE'),
    (8, 'TRUE'),
    (9, 'TRUE'),
    (10, 'FALSE'),
    (11, 'FALSE'),
    (12, 'FALSE'),
    (13, 'TRUE');

INSERT INTO employee(personid, gender, salary) VALUES
    (1, 'f', 5600.35),
    (2, 'f', 9500.00),
    (3, 'f', 4000.00),
    (4, 'f', 3200.90),
    (5, 'o', 11500.00),
    (6, 'f', 7200.30),
    (7, 'm', 6500.00),
    (8, 'm', 15000.50);

INSERT INTO bike(framenumber, brand, priceperday) VALUES
    (435643673, 'Scott', 120.50),
    (234236586, 'Giant', 50.70),
    (434234234, 'Scott', 500.90),
    (123465758, 'Scott', 430.00),
    (123687621, 'Cresta', 199.99),
    (345789234, 'Wheeler', 130.00),
    (145769043, 'Giant', 20.00),
    (888886543, 'Scott', 99.99),
    (235465890, 'Wheeler', 87.40);

INSERT INTO Reservation (reservationnumber, TotalPrice, CustomerID) VALUES
    (DEFAULT, 2321.67, 7),
    (DEFAULT, 7412.5, 8),
    (DEFAULT, 2570.49, 9),
    (DEFAULT, 10439.71, 10),
    (DEFAULT, 3870, 11),
    (DEFAULT, 120.5, 12),
    (DEFAULT, 2213.00, 9),
    (DEFAULT, 10129.00, 10),
    (DEFAULT, 3111, 8),
    (DEFAULT, 499.95, 13);

INSERT INTO ReservationPosition(reservationnumber, framenumber, fromdate, todate) VALUES
    (1, 434234234, '2019-09-01', '2019-09-03'),
    (1, 145769043, '2020-01-10', '2020-01-11'),
    (1, 888886543, '2020-02-15', '2020-02-28'),
    (2, 123465758, '2020-03-05', '2020-05-15'),
    (2, 435643673, '2020-01-01', '2020-12-31'),
    (3, 234236586, '2021-05-05', '2021-05-06'),
    (4, 345789234, '2020-09-01', '2020-09-30'),
    (4, 888886543, '2020-09-01', '2020-09-30'),
    (4, 123465758, '2020-09-01', '2020-09-30'),
    (5, 123465758, '2020-11-01', '2020-11-10'),
    (6, 435643673, '2020-12-15', '2020-12-16'),
    (7, 888886543, '2021-07-05', '2021-07-10');
