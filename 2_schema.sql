create table Person(
PersonID serial,
Name VARCHAR (55) NOT NULL,
Surname VARCHAR (55) NOT NULL,
Birthday DATE NOT NULL,
Telephone CHAR (10) NULL,
EMail VARCHAR (55) NULL,
Address VARCHAR (55) NOT NULL,
PRIMARY KEY (PersonID)
);

CREATE TYPE gender_enum as ENUM ('m','f','o');

create table Employee(
PersonID INTEGER NOT NULL,
Gender gender_enum NOT NULL,
Salary Decimal (8,2) NOT NULL,
PRIMARY KEY (PersonID)
);

create table Customer(
PersonID INTEGER NOT NULL,
Newsletter BOOLEAN NOT NULL,
PRIMARY KEY (PersonID)
);

create table Reservation(
ReservationNumber serial,
TotalPrice DECIMAL (7,2) NOT NULL,
CustomerID INTEGER NOT NUll,
PRIMARY KEY (ReservationNumber)
);

create table Bike(
FrameNumber INTEGER,
Brand VARCHAR (55) NOT NULL,
PricePerDay DECIMAL (6,2) NOT NULL,
PRIMARY KEY (FrameNumber)
);

create table ReservationPosition(
ReservationNumber INTEGER NOT NULL,
FrameNumber INTEGER NOT NULL,
FromDate TIMESTAMP NOT NULL,
ToDate TIMESTAMP NOT NULL,
PRIMARY KEY (ReservationNumber, FrameNumber)
);



	
	


